// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  nitro: {
    preset: 'netlify-edge',
    prerender: {
      crawlLinks: true, // recommended
    }
  },
  devtools: { enabled: true },
  modules: ["nuxt-og-image"],
})
